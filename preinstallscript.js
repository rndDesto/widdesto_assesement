var exec = require("child_process").exec;
var fs = require("fs");

var NPM_TOKEN;

fs.unlink(".npmrc", function (err) {
  if (err) {
    console.log("Delete File Fail.");
  } else {
    console.log("Delete File successfully.");
  }
});

fs.readFile(".env", "binary", function (err, data) {
  if (err) {
    console.error(err);
    return;
  }
  var result = data.toString().split("\n").map(function (e) {
    var parts = e.split("=");
    return { key: parts[0], value: parts[1] };
  });
  NPM_TOKEN = result.find(function (entry) {
    return entry.key === "NPM_TOKEN";
  }).value;
  var arrayList = [
    "@signal-angular-ui:registry=https://gitlab.com/api/v4/projects/47865110/packages/npm/",
    "//gitlab.com/api/v4/projects/47865110/packages/npm/:_authToken=" + NPM_TOKEN,
    "//gitlab.com/api/v4/projects/47865110/packages/npm/:always-auth=true",
    "//gitlab.com/api/v4/projects/47865110/packages/npm/:auto-install-peers=true",
  ];
  arrayList.forEach(function (e) {
    exec("echo " + e + "  >> .npmrc");
  });
  console.log(".npmrc is created, ready to getting private package");
});
