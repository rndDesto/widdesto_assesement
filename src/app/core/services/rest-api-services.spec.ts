import { TestBed } from '@angular/core/testing';
import { RestApiServices } from './rest-api-services';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('RestApiServices', () => {
  let service: RestApiServices;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RestApiServices]
    });
    service = TestBed.inject(RestApiServices);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should make a GET request', () => {
    const url = 'https://pokeapi.co/api/v2/pokemon';
    const dummyData = {
      results: [
        { name: 'bulbasaur', url: 'https://pokeapi.co/api/v2/pokemon/1/' },
        { name: 'ivysaur', url: 'https://pokeapi.co/api/v2/pokemon/2/' },
      ]
    };

    service.getRequest(url).subscribe(data => {
      expect(data).toEqual(dummyData);
    });

    const req = httpTestingController.expectOne(url);
    expect(req.request.method).toEqual('GET'); 

    req.flush(dummyData);
  });
});
