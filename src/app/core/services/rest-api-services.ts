/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class RestApiServices {
    constructor(
        private http: HttpClient
    ) {}

    getRequest(url: string, params?: any) {
        return this.http.get<any>(url,{params});
    }
}