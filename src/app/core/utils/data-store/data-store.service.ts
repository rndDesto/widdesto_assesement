import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class DataStoreService {
  getLocalStorageData<T>(key: string): T | null {
    const data = localStorage.getItem(key);
    return data ? JSON.parse(data) as T : null;
  }

  storeLocalStorageData<T>(key: string, data: T): void {
    localStorage.setItem(key, JSON.stringify(data));
  }

  removeLocalStorageData(key: string): void {
    localStorage.removeItem(key);
  }
}
