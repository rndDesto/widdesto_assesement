import { TestBed } from '@angular/core/testing';
import { DataStoreService } from './data-store.service';

describe('DataStoreService', () => {
  let service: DataStoreService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Test getLocalStorageData', () => {
    const methodSpy = spyOn(service, 'getLocalStorageData').and.callThrough();
    const data = {
      id: '1'
    }
    service.getLocalStorageData(data.id);
    expect(methodSpy).toHaveBeenCalled()
  });

  it('should return parsed data for an existing key', () => {
    const key = 'testKey';
    const testData = { foo: 'bar' };
    localStorage.setItem(key, JSON.stringify(testData));
    const result = service.getLocalStorageData(key);
    expect(result).toEqual(testData);
  });

  it('Test storeLocalStorageData', () => {
    const methodSpy = spyOn(service, 'storeLocalStorageData').and.callThrough();
    service.storeLocalStorageData('key', '');
    expect(methodSpy).toHaveBeenCalled()
  });


});
