import { TestBed } from "@angular/core/testing";
import { PokemonTypeColorService } from "./pokemon-type-color.service";

describe('PokemonTypeColorService', () => {
  let service: PokemonTypeColorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemonTypeColorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the correct color for known types (case-insensitive)', () => {
    expect(service.getTypeColor('Water')).toEqual('blue');
    expect(service.getTypeColor('ELECTRIC')).toEqual('yellow');
    expect(service.getTypeColor('grass')).toEqual('green');
  });

  it('should return gray for an unrecognized type', () => {
    expect(service.getTypeColor('unknown')).toEqual('gray');
  });

  it('should return gray for null or empty string input', () => {
    expect(service.getTypeColor('')).toEqual('gray');
  });

});