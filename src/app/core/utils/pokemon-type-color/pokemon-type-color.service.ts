import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PokemonTypeColorService {

  getTypeColor(type: string): string {
    const typeColorMap: {[key: string]: string} = {
        'normal': 'gray',
        'fire': 'red',
        'water': 'blue',
        'electric': 'yellow',
        'grass': 'green',
        'ice': 'cyan',
        'fighting': 'orange',
        'poison': 'purple',
        'ground': 'amber',
        'flying': 'sky',
        'psychic': 'pink',
        'bug': 'lime',
        'rock': 'stone',
        'ghost': 'indigo',
        'dragon': 'emerald',
        'dark': 'slate',
        'steel': 'zinc',
        'fairy': 'rose',
        'stellar': 'violet'
    };
    return typeColorMap[type.toLowerCase()] || 'gray';
}
}
