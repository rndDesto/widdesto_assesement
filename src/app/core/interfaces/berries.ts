import { PokemonMini } from './pokemons';

export interface BerriesLimitResponse {
  count: number;
  next: string;
  previous: string;
  results: PokemonMini[];
}

export interface Berries {
  firmness: Firmness;
  flavors: Flavor[];
  growth_time: number;
  id: number;
  item: Firmness;
  max_harvest: number;
  name: string;
  natural_gift_power: number;
  natural_gift_type: Firmness;
  size: number;
  smoothness: number;
  soil_dryness: number;
}

export interface BerriesDetail {
    name:    string;
    sprites: Sprites;
    firmness: string;
    category: Firmness;

}

export interface Firmness {
  name: string;
  url: string;
}

export interface Flavor {
  flavor: Firmness;
  potency: number;
}

export interface Sprites {
    default: string;
}


