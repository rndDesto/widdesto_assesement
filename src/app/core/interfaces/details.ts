import { HttpErrorResponse } from '@angular/common/http';

export type DetailStateBoolean = {
  detailPokemon: boolean;
  detailBerry: boolean;
};

export type ErrorMessageDetail = {
  msgPokemon: HttpErrorResponse | null;
  msgBerry: HttpErrorResponse | null;
};
