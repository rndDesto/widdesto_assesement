/* eslint-disable @typescript-eslint/no-explicit-any */
export interface PokeDexDetail {
  sprites: PokemonSprites;
  stats: PokemonStat[];
  weight: number;
  evolves_from_species: PokemonMini | null;
  evolutionChain: EvolutionChain[];
  name: string;
  id: number;
  customWeight: number;
  types: string;
}

export interface PokemonItemType {
  sprites: PokemonSprites;
  name: string;
  id: number;
  types: PokemonType[];
}

export interface EvolutionChain {
  species: PokemonMini;
  sprites: PokemonSprites;
  weight: number;
  id: number;
}

export interface Pokemon {
  abilities?: PokemonAbility[];
  base_experience?: number;
  forms?: PokemonMini[];
  game_indices?: GenerationGameIndex[];
  height?: number;
  held_items?: PokemonHeldItem[];
  id: number;
  is_default?: boolean;
  location_area_encounters?: string;
  moves?: PokemonMove[];
  name: string;
  order?: number;
  species: PokemonMini;
  sprites: PokemonSprites;
  stats: PokemonStat[];
  types: PokemonType[];
  weight: number;
  date?: Date | string;
  favorite_date?: Date | string;
}

export interface GenerationGameIndex {
  game_index: number;
  generation: PokemonMini;
}

export interface PokemonMini {
  name: string;
  url: string;
}

export interface PokemonType {
  type: PokemonMini;
  slot: number;
}

export interface PokemonAbility {
  is_hidden: boolean;
  slot: number;
  ability: PokemonMini;
}

export interface PokemonStat {
  stat: PokemonMini;
  effort: number;
  base_stat: number;
}

export interface PokemonSprites {
  front_default?: string | null;
  front_shiny?: string;
  front_female?: string;
  front_shiny_female?: string;
  back_default?: string;
  back_shiny?: string;
  back_female?: string;
  back_shiny_female?: string;
  other: PokemonSpritesOther;
}

export interface PokemonSpritesOther {
  'official-artwork': PokemonSpritesOtherOfficialArtwork;
}

export interface PokemonSpritesOtherOfficialArtwork {
  front_default: string;
}



export type SpritesTypes = keyof PokemonSprites;

export enum PokemonTypesEnum {
  bug = 'bug',
  dark = 'dark',
  dragon = 'dragon',
  electric = 'electric',
  fire = 'fire',
  fairy = 'fairy',
  fighting = 'fighting',
  flying = 'flying',
  ghost = 'ghost',
  grass = 'grass',
  ground = 'ground',
  ice = 'ice',
  normal = 'normal',
  poison = 'poison',
  psychic = 'psychic',
  rock = 'rock',
  steel = 'steel',
  water = 'water'
}

export interface PokemonMove {
  move: PokemonMini;
  version_group_details: PokemonMoveVersion[];
}

export interface PokemonMoveVersion {
  version_group: PokemonMini;
  move_learn_method: PokemonMini;
  level_learned_at: number;
}

export interface PokemonHeldItem {
  item: PokemonMini;
  version_details: PokemonHeldItemVersion;
}

export interface PokemonHeldItemVersion {
  version: PokemonMini;
  rarity: number;
}
export interface PokemonLimitResponse {
  count: number;
  next: string;
  previous: string;
  results: PokemonMini[];
}

export type Pokedex = Pokemon & {
  past_abilities: any[];
  past_types: any[];
  evolution_chain: PokedexEvolutionChain;
};

export interface Ability {
  ability: Form;
  is_hidden: boolean;
  slot: number;
}

export interface Form {
  name: string;
  url: string;
}

export interface PokedexEvolutionChain {
  baby_trigger_item: null;
  chain: EvolChain;
  id: number;
}

export interface EvolChain {
  evolution_details: EvolutionDetail[];
  evolves_to: EvolChain[];
  is_baby: boolean;
  species: Form;
}

export interface EvolutionDetail {
  gender: null;
  held_item: null;
  item: null;
  known_move: null;
  known_move_type: null;
  location: null;
  min_affection: null;
  min_beauty: null;
  min_happiness: null;
  min_level: number;
  needs_overworld_rain: boolean;
  party_species: null;
  party_type: null;
  relative_physical_stats: null;
  time_of_day: string;
  trade_species: null;
  trigger: Form;
  turn_upside_down: boolean;
}

export interface GameIndex {
  game_index: number;
  version: Form;
}

export interface Move {
  move: Form;
  version_group_details: VersionGroupDetail[];
}

export interface VersionGroupDetail {
  level_learned_at: number;
  move_learn_method: Form;
  version_group: Form;
}

export interface Species {
  base_happiness: number;
  capture_rate: number;
  color: Form;
  egg_groups: Form[];
  evolution_chain: SpeciesEvolutionChain;
  evolves_from_species: null;
  flavor_text_entries: FlavorTextEntry[];
  form_descriptions: any[];
  forms_switchable: boolean;
  gender_rate: number;
  genera: Genus[];
  generation: Form;
  growth_rate: Form;
  habitat: Form;
  has_gender_differences: boolean;
  hatch_counter: number;
  id: number;
  is_baby: boolean;
  is_legendary: boolean;
  is_mythical: boolean;
  name: string;
  names: Name[];
  order: number;
  pal_park_encounters: PalParkEncounter[];
  pokedex_numbers: PokedexNumber[];
  shape: Form;
  varieties: Variety[];
}

export interface SpeciesEvolutionChain {
  url: string;
}

export interface FlavorTextEntry {
  flavor_text: string;
  language: Form;
  version: Form;
}

export interface Genus {
  genus: string;
  language: Form;
}

export interface Name {
  language: Form;
  name: string;
}

export interface PalParkEncounter {
  area: Form;
  base_score: number;
  rate: number;
}

export interface PokedexNumber {
  entry_number: number;
  pokedex: Form;
}

export interface Variety {
  is_default: boolean;
  pokemon: Form;
}

export interface GenerationV {
  'black-white': Sprites;
}

export interface GenerationIv {
  'diamond-pearl': Sprites;
  'heartgold-soulsilver': Sprites;
  platinum: Sprites;
}

export interface Versions {
  'generation-i': GenerationI;
  'generation-ii': GenerationIi;
  'generation-iii': GenerationIii;
  'generation-iv': GenerationIv;
  'generation-v': GenerationV;
  'generation-vi': { [key: string]: Home };
  'generation-vii': GenerationVii;
  'generation-viii': GenerationViii;
}

export interface Other {
  dream_world: DreamWorld;
  home: Home;
  'official-artwork': OfficialArtwork;
  showdown: Sprites;
}

export interface Sprites {
  back_default: string;
  back_female: null;
  back_shiny: string;
  back_shiny_female: null;
  front_default: string;
  front_female: null;
  front_shiny: string;
  front_shiny_female: null;
  other?: Other;
  versions?: Versions;
  animated?: Sprites;
}

export interface GenerationI {
  'red-blue': RedBlue;
  yellow: RedBlue;
}

export interface RedBlue {
  back_default: string;
  back_gray: string;
  back_transparent: string;
  front_default: string;
  front_gray: string;
  front_transparent: string;
}

export interface GenerationIi {
  crystal: Crystal;
  gold: Gold;
  silver: Gold;
}

export interface Crystal {
  back_default: string;
  back_shiny: string;
  back_shiny_transparent: string;
  back_transparent: string;
  front_default: string;
  front_shiny: string;
  front_shiny_transparent: string;
  front_transparent: string;
}

export interface Gold {
  back_default: string;
  back_shiny: string;
  front_default: string;
  front_shiny: string;
  front_transparent?: string;
}

export interface GenerationIii {
  emerald: OfficialArtwork;
  'firered-leafgreen': Gold;
  'ruby-sapphire': Gold;
}

export interface OfficialArtwork {
  front_default: string;
  front_shiny: string;
}

export interface Home {
  front_default: string;
  front_female: null;
  front_shiny: string;
  front_shiny_female: null;
}

export interface GenerationVii {
  icons: DreamWorld;
  'ultra-sun-ultra-moon': Home;
}

export interface DreamWorld {
  front_default: string;
  front_female: null;
}

export interface GenerationViii {
  icons: DreamWorld;
}

export interface Stat {
  base_stat: number;
  effort: number;
  stat: Form;
}

export interface Type {
  slot: number;
  type: Form;
}


export interface getPokemonParam{
  limit:number,
  page:number
}
