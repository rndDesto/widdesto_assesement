import { HttpErrorResponse } from "@angular/common/http";
import { throwError, Observable } from "rxjs";

export const handleError = (error: Error | HttpErrorResponse): Observable<never> => {
  return throwError(() => error);
};
