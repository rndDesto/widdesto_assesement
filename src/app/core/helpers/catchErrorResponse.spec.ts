import { HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";
import { catchError } from 'rxjs/operators';
import { handleError } from "./catchErrorResponse";

describe('handleError Function', () => {
  

  it('should handle HttpErrorResponse correctly', (done: DoneFn) => {
    const expectedHttpError = new HttpErrorResponse({
      status: 404,
      statusText: 'Not Found',
      url: 'http://example.com',
      error: 'Not found'
    });

    throwError(() => expectedHttpError).pipe(
      catchError(handleError)
    ).subscribe({
      next: () => fail('Should have failed with the expected HTTP error'),
      error: (error) => {
        expect(error).toBe(expectedHttpError);
        done();
      }
    });
  });
});
