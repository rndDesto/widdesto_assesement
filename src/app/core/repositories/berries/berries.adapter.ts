import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { pokemonApi } from "../../../../environments/pokemonApi";
import { BerriesLimitResponse } from "../../interfaces/berries";
import { RestApiServices } from "../../services/rest-api-services";

@Injectable({
  providedIn: 'root',
})

export class BerriesAdapter {
  constructor(
      private apiService: RestApiServices
  ) {}

  berryLists(): Observable<BerriesLimitResponse> {
    return this.apiService.getRequest(`${pokemonApi}/berry?limit=64`, null)
  }

  berryUrls<T>(url:string): Observable<T> {
    return this.apiService.getRequest(`${url}`, null)
  }

}