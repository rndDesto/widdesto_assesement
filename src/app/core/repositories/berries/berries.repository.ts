import { Observable } from 'rxjs';
import { BerriesDetail } from '../../interfaces/berries';

export abstract class BerriesRepository {
  abstract getBerryLists(): Observable<BerriesDetail[]>;
  abstract getBerryByUrls<T>(params:string): Observable<T>;
}