import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { pokemonApi } from "../../../../environments/pokemonApi";
import { RestApiServices } from "../../services/rest-api-services";
import { EvolChain, Pokemon, PokemonLimitResponse, getPokemonParam } from "../../interfaces/pokemons";

@Injectable({
  providedIn: 'root',
})

export class PokemonAdapter {
  constructor(
      private apiService: RestApiServices
  ) {}

  pokemonLists(params:getPokemonParam): Observable<PokemonLimitResponse> {
    return this.apiService.getRequest(`${pokemonApi}/pokemon?limit=${params.limit}&offset=${params.page}`)
  }

  pokemonByNameOrId(name:string): Observable<Pokemon> {
    return this.apiService.getRequest(`${pokemonApi}/pokemon/${name}`)
  }

  pokemonByUrl<T>(name:string): Observable<T> {
    return this.apiService.getRequest(`${name}`)
  }

  pokemonNextEvolution(speciesName: string, evolutionData: EvolChain): string[] {
    if (evolutionData.species.name.toLowerCase() === speciesName.toLowerCase()) {
        return evolutionData.evolves_to.map((evolve: EvolChain) => {
            return evolve.species.name;
        });
    }
    
    for (const evolve of evolutionData.evolves_to) {
        const result = this.pokemonNextEvolution(speciesName, evolve);
        if (result.length > 0) {
            return result;
        }
    }

    return []; 
  }

}