import { Observable } from 'rxjs';
import { PokeDexDetail, Pokemon, getPokemonParam } from '../../interfaces/pokemons';
export abstract class PokemonRepository {
  abstract getPokemonLists(params:getPokemonParam): Observable<Pokemon[]>;
  abstract getPokemonByUrls<T>(params:string): Observable<T>;
  abstract getPokemonByNameOrIds(params:string): Observable<Pokemon>;
  abstract getCombinedPokemons(params:string): Observable<PokeDexDetail>;
}
