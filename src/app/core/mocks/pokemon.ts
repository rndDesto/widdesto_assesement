import { Pokemon, PokemonItemType, PokemonSpritesOther } from "../interfaces/pokemons";

export const mockPokemonsData: Pokemon[] = [
    {
      id: 1,
      name: 'Pikachu',
      species: { name: 'pikachu', url: 'https://pokeapi.co/api/v2/pokemon-species/25/' },
      sprites: {
        back_default: 'https://pokeapi.co/api/v2/sprites/pokemon/back/25.png',
        back_shiny: 'https://pokeapi.co/api/v2/sprites/pokemon/back/shiny/25.png',
        front_default: 'https://pokeapi.co/api/v2/sprites/pokemon/25.png',
        front_female: 'https://pokeapi.co/api/v2/sprites/pokemon/female/25.png',
        front_shiny: 'https://pokeapi.co/api/v2/sprites/pokemon/shiny/25.png',
        front_shiny_female: '',
        back_female: '',
        back_shiny_female: '',
        other: {} as PokemonSpritesOther
      },
      stats: [],
      types: [],
      weight: 60,
    },
  ];


  export const pokemonItemMock: PokemonItemType = {
    id: 1,
    name: 'Pikachu',
    types:  [
      {
        "slot": 1,
        "type": {
          "name": "normal",
          "url": "https://pokeapi.co/api/v2/type/1/"
        }
      }
    ],
    sprites: {
      other: {
        'official-artwork':{
          front_default: 'https://pokeapi.co/api/v2/pokemon/25'
        }
      }
    }
  };