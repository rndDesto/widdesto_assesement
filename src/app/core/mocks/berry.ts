import { BerriesDetail } from "../interfaces/berries";

export const mockBerriesDetail: BerriesDetail = {
  "name": "cheri",
  "sprites": {
      "default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/items/cheri-berry.png"
  },
  "firmness": "soft",
  "category": {
      "name": "medicine",
      "url": "https://pokeapi.co/api/v2/item-category/3/"
  }
};
