import { Injectable } from '@angular/core';
import { PokeDexDetail, PokedexEvolutionChain, Pokemon, PokemonMini, Species, getPokemonParam } from '../interfaces/pokemons';
import { PokemonRepository } from '../repositories/pokemon/pokemon.repository';
import { PokemonAdapter } from '../repositories/pokemon/pokemon.adapter';
import { Observable, forkJoin, of } from 'rxjs';
import { catchError, map, switchMap, take, timeout } from 'rxjs/operators';
import { handleError } from '../helpers/catchErrorResponse';

@Injectable({
  providedIn: 'root'
})
export class PokemonUsecase implements PokemonRepository {
    private requestTimeout = 3000;
    constructor(private services: PokemonAdapter) {}

    getPokemonByUrls<T>(param:string): Observable<T> {
        return this.services.pokemonByUrl(param);
    }

    getPokemonByNameOrIds(params:string): Observable<Pokemon> {
        return this.services.pokemonByNameOrId(params);
    }

    getPokemonLists(params:getPokemonParam): Observable<Pokemon[]> {
        return this.services.pokemonLists({page:params.page,limit:params.limit}).pipe(
            take(1),
            switchMap((response) => {
            if (!response.next) {
                return of([]);
            }
            const requests = response.results.map((result: PokemonMini) =>
                this.getPokemonByUrls<Pokemon>(result.url).pipe(
                take(1),
                timeout(this.requestTimeout),
                catchError(handleError)
                )
            );
            return forkJoin(requests);
            }),
            timeout(this.requestTimeout),
            catchError(handleError)
        );
    }

    getCombinedPokemons(name: string): Observable<PokeDexDetail>{
        return this.getPokemonByNameOrIds(name).pipe(
            switchMap((response: Pokemon) => {
                return this.getPokemonByUrls<Species>(response.species.url).pipe(
                    switchMap((species: Species) => {
                        return this.getPokemonByUrls<PokedexEvolutionChain>(species.evolution_chain.url).pipe(
                            switchMap((evolution_chain: PokedexEvolutionChain) => {
                                const evolutionNames = this.services.pokemonNextEvolution(response.name, evolution_chain.chain); 

                                const requestsEvolution = evolutionNames.length > 0 ? evolutionNames.map((name: string) => {
                                    return this.getPokemonByNameOrIds(`${name}`);
                                }) : [this.getPokemonByNameOrIds(`${response.name}`)];

                                return forkJoin(requestsEvolution).pipe(
                                    map(evolist => {

                                        const lastResponse = evolist[0] as Pokemon;
                                        const lastEvo = response.name === lastResponse.species.name

                                        return {
                                            name: response.name,
                                            types: response.types[0].type.name,
                                            id: response.id,
                                            sprites: response.sprites,
                                            stats: response.stats,
                                            weight: response.weight,
                                            customWeight: response.weight,
                                            evolves_from_species: species.evolves_from_species,
                                            evolutionChain: !lastEvo ? evolist :[]
                                        };
                                    }),
                                    timeout(this.requestTimeout),
                                    catchError(handleError)
                                );
                            })
                        );
                    })
                );
            })
        )
    }
}