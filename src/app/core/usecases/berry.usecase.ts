import { Injectable } from '@angular/core';
import { catchError, forkJoin, map, switchMap, take, timeout, Observable } from 'rxjs';
import { Berries, BerriesDetail, BerriesLimitResponse } from '../interfaces/berries';
import { PokemonMini } from '../interfaces/pokemons';
import { handleError } from '../helpers/catchErrorResponse';
import { BerriesAdapter } from '../repositories/berries/berries.adapter';
import { BerriesRepository } from '../repositories/berries/berries.repository';

@Injectable({
  providedIn: 'root'
})
export class BerryUsecase implements BerriesRepository {
  
  private requestTimeout = 3000;
  constructor(private services: BerriesAdapter) {}

  getBerryLists(): Observable<BerriesDetail[]> {
    return this.services.berryLists().pipe(
      take(1),
      switchMap((response:BerriesLimitResponse) => {
        const requests = response.results.map((result: PokemonMini) =>
          this.getBerryByUrls<Berries>(result.url).pipe(
            take(1),
            switchMap((berryList: Berries) => {
              return this.getBerryByUrls<BerriesDetail>(berryList.item.url).pipe(
                take(1),
                map((item: BerriesDetail) => ({ 
                  name: berryList.name,
                  sprites: item.sprites,
                  firmness: berryList.firmness.name,
                  category: item.category,
                }))
              );
            }),
            timeout(this.requestTimeout),
            catchError(handleError)
          )
        );
        return forkJoin(requests);
      }),
      timeout(this.requestTimeout),
      catchError(handleError)
    );
  }

  getBerryByUrls<T>(url:string): Observable<T> {
    return this.services.berryUrls(url);
  }

}
