import { CommonModule, isPlatformBrowser } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { PokeDexDetail, PokedexEvolutionChain } from '../../core/interfaces/pokemons';
import { BerriesDetail } from '../../core/interfaces/berries';
import { CalloutComponent } from '../../components/callout/callout.component';
import { DetailStateBoolean, ErrorMessageDetail } from '../../core/interfaces/details';
import { LoadingComponent } from '../../components/loading/loading.component';
import { PLATFORM_ID, Inject } from '@angular/core';
import { PokemonTypeColorService } from '../../core/utils/pokemon-type-color/pokemon-type-color.service';
import { SignalButtonModule, SignalCalloutModule, SignalIconModule } from '@signal-angular-ui/core';
import { PokemonUsecase } from '../../core/usecases/pokemon.usecase';
import { BerryUsecase } from '../../core/usecases/berry.usecase';
import { BerryItemComponent } from '../../components/berry-item/berry-item.component';
import { DataStoreService } from '../../core/utils/data-store/data-store.service';

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [RouterModule, CommonModule, CalloutComponent, LoadingComponent, SignalButtonModule, SignalIconModule, SignalCalloutModule, BerryItemComponent],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss'
})
export class DetailComponent implements OnInit{
  pokemon: PokeDexDetail | null = null;
  berryList: BerriesDetail[] | null = null;
  pokemonEvolutionChain: PokedexEvolutionChain | null = null; 
  isLoading:DetailStateBoolean = {
    detailPokemon: false,
    detailBerry: false,
  }
  isError:DetailStateBoolean = {
    detailPokemon: false,
    detailBerry: false,
  }
  errorMessage: ErrorMessageDetail = {
    msgPokemon: null,
    msgBerry: null
  };

  currentWeightChange: number = 0;
  showWeightChange: boolean = false;
  animationStatus: boolean = false;
  
  constructor(
    private router: Router,
    private pokemonDetailService: PokemonUsecase,
    private pokemonTypeColorService: PokemonTypeColorService,
    private berryService: BerryUsecase,
    private dataStorage:DataStoreService,
    @Inject(PLATFORM_ID) private platformId: object
  ) {
  }

  ngOnInit(): void {
    if(isPlatformBrowser(this.platformId)){
      if(this.dataStorage.getLocalStorageData('pokemon') === null){
        this.handleResetPokemon();
      }else{
        this.pokemon = this.dataStorage.getLocalStorageData('pokemon');
      }
    }
    this.getAllBerries();
  }
  

  handleResetPokemon(){
    this.dataStorage.removeLocalStorageData('pokemon');
    this.router.navigate(['/']);
  }

  handleNextEvolve(name: string){
    this.pokemonDetailService.getCombinedPokemons(name).subscribe(
    {
      next: (data) => {
        this.isError.detailPokemon = false;
        this.isLoading.detailPokemon = false;
        this.pokemon = data as PokeDexDetail;
        this.dataStorage.storeLocalStorageData('pokemon',data);
      },
      error: (error) => {
        this.isError.detailPokemon = true;
        this.errorMessage.msgPokemon = error as HttpErrorResponse;
        this.isLoading.detailPokemon = false;
      }
    });
  }

  getAllBerries(){
    this.isLoading.detailBerry = true;
    this.errorMessage.msgBerry = null;
    this.isError.detailBerry = false;
    this.berryService.getBerryLists().subscribe(
      {
        next:(data) =>{
          this.isError.detailBerry = false;
          this.isLoading.detailBerry = false;
          this.berryList = data as BerriesDetail[];
        },
        error:(error) =>{
          this.isError.detailBerry = true;
          this.errorMessage.msgBerry = error as HttpErrorResponse;
          this.isLoading.detailBerry = false;
        }
      });
  }

  handleFeedPokemon(berry: BerriesDetail) {
    if (this.pokemon) {
      const berryType = berry.category.name;
      const isNegativeChange = berryType === 'effort-drop' || berryType === 'baking-only';
      const firmnessWeightMap: { [key: string]: number } = {
        'very-soft': 15,
        'soft': 5,
        'hard': 15,
        'very-hard': 20,
        'super-hard': 50,
        'unknown': 0
      };
  
      let weightChange = firmnessWeightMap[berry.firmness] || 0;
      weightChange = isNegativeChange ? -weightChange : weightChange;

      this.currentWeightChange = weightChange;
      this.showWeightChange = true;
  
      const newWeight = Math.max(this.pokemon.weight, this.pokemon.customWeight + weightChange);
      this.pokemon.customWeight = newWeight;
      this.animationStatus = true;
      this.dataStorage.storeLocalStorageData('pokemon',this.pokemon);
      this.shuffleBerry(this.berryList ?? []);
    }
  }

  onAnimationEnd() {
    this.showWeightChange = false;
    this.animationStatus = false;
  }
  

  getPokemonClass(color: string | undefined){
    if(color){
      return `bg-${this.pokemonTypeColorService.getTypeColor(color)}-500`
    }
    return 'bg-white'
  }


  calculateProgressBarWidth(customWeight: number, maxWeight: number): string {
    const initialWeight = this.pokemon?.weight ?? 0;
    if (customWeight >= maxWeight) {
      return '100';
    }
    const progress = ((customWeight - initialWeight) / (maxWeight - initialWeight)) * 100;
    const boundedProgress = Math.min(Math.max(progress, 0), 100);
  
    return Number(boundedProgress.toFixed(1)).toString() ;
  }

  shuffleBerry<T>(berries: T[]): T[] {
    for (let i = berries.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [berries[i], berries[j]] = [berries[j], berries[i]]; 
    }
    return berries;
  }

}
