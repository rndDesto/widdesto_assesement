import { Component, Inject, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { Pokemon, PokemonItemType } from '../../core/interfaces/pokemons';
import { CommonModule, isPlatformBrowser } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { CdkVirtualScrollViewport, ScrollingModule } from '@angular/cdk/scrolling';
import { CalloutComponent } from '../../components/callout/callout.component';
import { LoadingComponent } from '../../components/loading/loading.component';
import { SignalButtonModule, SignalFormModule, SignalImagesModule } from '@signal-angular-ui/core';
import { PokemonUsecase } from '../../core/usecases/pokemon.usecase';
import { PokemonItemComponent } from '../../components/pokemon-item/pokemon-item.component';
import { DataStoreService } from '../../core/utils/data-store/data-store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  standalone: true,
  imports:[
    HttpClientModule, 
    CommonModule, 
    RouterModule, 
    ScrollingModule, 
    CalloutComponent, 
    LoadingComponent, 
    SignalButtonModule, 
    SignalFormModule,
    SignalImagesModule,
    PokemonItemComponent
  ],

})
export class HomeComponent implements OnInit {
  @ViewChild('viewport') viewportRef: CdkVirtualScrollViewport | undefined;
  
  pokemons: Pokemon[] = [];
  selectedPokemon: PokemonItemType | null = null;
  currentPage: number = 0;
  pageSize: number = 20;
  isEndOfList: boolean = false;
  isLoading:boolean = false;
  isLoadingChosenPokemon:boolean = false;
  isError:boolean = false;
  errorMesssage:HttpErrorResponse | null = null
  isBrowser: boolean;
  search_value: string = '';

  constructor(
    private router: Router,
    private pokemonService: PokemonUsecase,
    private dataStorage:DataStoreService,
    @Inject(PLATFORM_ID) private platformId: object) {
      this.isBrowser = isPlatformBrowser(this.platformId);
    }

  ngOnInit() {
    this.loadPokemonList();
    if(this.isBrowser && this.dataStorage.getLocalStorageData('pokemon') !== null){
        this.router.navigate(['/detail']);
    }
  }

  loadPokemonList() {
    this.isLoading = true;
    this.isError = false;
    this.errorMesssage = null;
    
    this.pokemonService.getPokemonLists({
      limit: this.pageSize,
      page: this.currentPage
    }).subscribe({
      next: (data) => {
        if (data.length < this.pageSize) {
          this.isEndOfList = true;
        }
        this.pokemons = this.pokemons.concat(data);
        this.isLoading = false;
        this.isError = false;
        this.currentPage += this.pageSize;
      },
      error: (error) => {
        this.isError = true;
        this.errorMesssage = error as HttpErrorResponse;
        this.isLoading = false;
      }
    });
  }

  handleScroll() {
    const viewport = this.viewportRef;
    const endPosition = viewport?.measureScrollOffset("bottom") || 0;
    if (endPosition < 100 && !this.isEndOfList && !this.isLoading  && !this.isError) {
      this.loadPokemonList();
    }
    else if(endPosition > 600){
      this.isError = false;
    }
  }

  handleSelectedPokemon(pokemon: PokemonItemType) {
    this.selectedPokemon = pokemon;
  }

  handleChoosePokemon() {
      if (this.selectedPokemon) {
        this.fetchAndNavigate(this.selectedPokemon.name);
      }
  }

  handleSearchPokemon(){
    if(this.search_value !== ''){
      this.fetchAndNavigate(this.search_value);
    }
  }

  fetchAndNavigate(pokemonNameOrId: string) {
    this.isLoadingChosenPokemon = true;
    this.isError = false;
    this.errorMesssage = null;
    this.pokemonService.getCombinedPokemons(pokemonNameOrId).subscribe({
      next: (data) => {
        this.dataStorage.storeLocalStorageData('pokemon', data);
        this.router.navigate(['/detail']);
        this.isLoadingChosenPokemon = false;
      },
      error: (error) => {
        this.isError = true;
        this.errorMesssage = error as HttpErrorResponse;
        this.isLoadingChosenPokemon = false;
      }
    })
  }

  closeCallout() {
    this.isError = false;
    this.errorMesssage = null;
  }

}