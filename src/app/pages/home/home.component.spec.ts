/* eslint-disable @typescript-eslint/no-unused-vars */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { PokemonUsecase } from '../../core/usecases/pokemon.usecase';
import { HomeComponent } from './home.component';
import { of } from 'rxjs';
import { pokemonItemMock } from '../../core/mocks/pokemon';
import { isPlatformBrowser } from '@angular/common';
import { PLATFORM_ID } from '@angular/core';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let pokemonService: PokemonUsecase;

  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ HomeComponent ],
      providers: [ 
        PokemonUsecase, 
        { provide: Router, useValue: { navigate: jasmine.createSpy('navigate') } } 
      ]
    })
    .compileComponents();
  
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    pokemonService = TestBed.inject(PokemonUsecase);
    router = TestBed.inject(Router);
  });

  it('Component : HomeComponent Snapshot', () => {
    expect(component).toBeTruthy();
  });

  
  it('load pokemon list and not navigate if pokemon is not in localStorage', () => {
    spyOn(component, 'loadPokemonList');
    spyOn(localStorage, 'getItem').and.returnValue(null);
    component.ngOnInit();
    expect(component.loadPokemonList).toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();
  });

  it('handle scroll and load more pokemon list', () => {
    spyOn(component, 'loadPokemonList');
    component.isEndOfList = false;
    component.isLoading = false;
    component.isError = false;
    component.handleScroll();
    expect(component.loadPokemonList).toHaveBeenCalled();
  });

  it('handle closeCallout', () => {
    component.closeCallout();
    expect(component.selectedPokemon).toBeNull();
  });

  it('should handleChoosePokemon', () => {
    component.selectedPokemon = pokemonItemMock;
    const fetchAndNavigateSpy = spyOn(component, 'fetchAndNavigate');
    component.handleChoosePokemon();
    expect(fetchAndNavigateSpy).toHaveBeenCalled();
  });


  it('should not call fetchAndNavigate if selectedPokemon is null', () => {
    component.selectedPokemon = null;
    const fetchAndNavigateSpy = spyOn(component, 'fetchAndNavigate').and.returnValue(undefined);
    component.handleChoosePokemon();
    expect(fetchAndNavigateSpy).not.toHaveBeenCalled();
  });

  it('should handleSearch', () => {
    component.search_value = 'pikachu';
    const fetchAndNavigateSpy = spyOn(component, 'fetchAndNavigate');
    component.handleSearchPokemon();
    expect(fetchAndNavigateSpy).toHaveBeenCalled();
  });

  it('should set selectedPokemon correctly', () => {
    component.handleSelectedPokemon(pokemonItemMock);
    expect(component.selectedPokemon).toEqual(pokemonItemMock);
  });
  
});