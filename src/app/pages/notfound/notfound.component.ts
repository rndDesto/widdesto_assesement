import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SignalButtonModule } from '@signal-angular-ui/core';
import { DataStoreService } from '../../core/utils/data-store/data-store.service';

@Component({
  selector: 'app-notfound',
  standalone: true,
  imports: [SignalButtonModule],
  templateUrl: './notfound.component.html',
  styleUrl: './notfound.component.scss'
})
export class NotfoundComponent {
  constructor(
    private router: Router,
    private dataStorage:DataStoreService
  ) {}
  toHome(){
    this.router.navigate(['/']);
    this.dataStorage.removeLocalStorageData('pokemon');
  }

}
