import { CommonModule } from '@angular/common';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SignalSnackbarModule } from '@signal-angular-ui/core';

@Component({
  selector: 'app-callout',
  standalone: true,
  imports: [CommonModule,SignalSnackbarModule],
  templateUrl: './callout.component.html',
  styleUrl: './callout.component.scss'
})
export class CalloutComponent {
  @Input() open: boolean = false;
  @Input() message: string | undefined = '';
  @Output() handleClose: EventEmitter<void> = new EventEmitter();

  closeComponent() {
    this.handleClose.emit();
  }
}
