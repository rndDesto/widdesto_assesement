import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalloutComponent } from './callout.component';

describe('CalloutComponent', () => {
  let component: CalloutComponent;
  let fixture: ComponentFixture<CalloutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CalloutComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CalloutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emit close on handleClose', () => {
    spyOn(component.handleClose, 'emit');
    component.closeComponent();
    expect(component.handleClose.emit).toHaveBeenCalled();
  });
});
