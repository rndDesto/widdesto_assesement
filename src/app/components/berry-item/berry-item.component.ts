import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BerriesDetail } from '../../core/interfaces/berries';

@Component({
  selector: 'app-berry-item',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './berry-item.component.html',
  styleUrl: './berry-item.component.scss'
})
export class BerryItemComponent{
  @Output() feedPokemon = new EventEmitter<BerriesDetail>();
  @Input() berry!: BerriesDetail;

  constructor() { }

  handleFeedPokemon(berries:BerriesDetail)     {
    this.feedPokemon.emit(berries);
  }

}
