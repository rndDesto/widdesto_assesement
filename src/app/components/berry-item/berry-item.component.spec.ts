import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BerryItemComponent } from './berry-item.component';
import { mockBerriesDetail } from '../../core/mocks/berry';

describe('BerryItemComponent', () => {
  let component: BerryItemComponent;
  let fixture: ComponentFixture<BerryItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BerryItemComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BerryItemComponent);
    component = fixture.componentInstance;
    component.berry = mockBerriesDetail;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emit feedPokemon on handleFeedPokemon', () => {
    spyOn(component.feedPokemon, 'emit');
    component.handleFeedPokemon(mockBerriesDetail);
    expect(component.feedPokemon.emit).toHaveBeenCalledWith(mockBerriesDetail);
  });
});
