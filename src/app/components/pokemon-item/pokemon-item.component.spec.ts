import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PokemonItemComponent } from './pokemon-item.component';
import { CommonModule } from '@angular/common';
import { PokemonTypeColorService } from '../../core/utils/pokemon-type-color/pokemon-type-color.service';
import { pokemonItemMock } from '../../core/mocks/pokemon';

describe('PokemonItemComponent', () => {
  let component: PokemonItemComponent;
  let fixture: ComponentFixture<PokemonItemComponent>;
  let mockPokemonTypeColorService: Partial<PokemonTypeColorService>;

  beforeEach(async () => {
    mockPokemonTypeColorService = {
      getTypeColor: () => 'red'
    };

    await TestBed.configureTestingModule({
      imports: [CommonModule, PokemonItemComponent],
      providers: [
        { provide: PokemonTypeColorService, useValue: mockPokemonTypeColorService }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PokemonItemComponent);
    component = fixture.componentInstance;
  });

  it('emit selectPokemon on handleSelectedPokemon', () => {
    spyOn(component.selectPokemon, 'emit');
    component.handleSelectedPokemon(pokemonItemMock);
    expect(component.selectPokemon.emit).toHaveBeenCalledWith(pokemonItemMock);
  });

  it('getPokemonClass', () => {
    const color = 'electric';
    expect(component.getPokemonClass(color)).toContain('bg-red-500');
    expect(component.getPokemonClass('')).toBe('bg-gray-500');
  });
  
});
