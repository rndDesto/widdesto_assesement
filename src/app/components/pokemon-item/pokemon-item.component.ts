import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PokemonItemType } from '../../core/interfaces/pokemons';
import { PokemonTypeColorService } from '../../core/utils/pokemon-type-color/pokemon-type-color.service';

@Component({
  selector: 'app-pokemon-item',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './pokemon-item.component.html',
  styleUrl: './pokemon-item.component.scss'
})
export class PokemonItemComponent {
  @Output() selectPokemon = new EventEmitter<PokemonItemType>();
  @Input() pokemon!: PokemonItemType;
  @Input() isSelected: boolean = false;

  constructor(
    private pokemonTypeColorService: PokemonTypeColorService,
  ) { }

  handleSelectedPokemon(pokemon: PokemonItemType) {
    this.selectPokemon.emit(pokemon);
  }

  getPokemonClass(color: string){
    if(color){
      return `bg-${this.pokemonTypeColorService.getTypeColor(color)}-500`
    }
    return 'bg-gray-500'
  }

}
