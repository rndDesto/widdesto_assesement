import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule),
        title: 'Home',
    },
    {
        path: 'detail',
        loadChildren: () => import('./pages/detail/detail.module').then((m) => m.DetailModule),
        title: 'Detail',
    },
    {
        path: '**',
        loadChildren: () => import('./pages/notfound/notfound.module').then((m) => m.NotfoundModule),
        title: 'Notfound',
    },
];
