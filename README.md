# WiddestoAssesment

## Prerequisite
pastikan persyaratan tersebut telah diinstall untuk mejalankan aplikasi
```node versi 20```,
```angular/cli```,
```angular-http-server```
kemudian buat file pada root directory dengan nama .npmrc, lalu paste code bertikut

```
@signal-angular-ui:registry=https://gitlab.com/api/v4/projects/47865110/packages/npm/  
//gitlab.com/api/v4/projects/47865110/packages/npm/:_authToken=glpat-wamN9eFecsFetr6SWQQQ
//gitlab.com/api/v4/projects/47865110/packages/npm/:always-auth=true
//gitlab.com/api/v4/projects/47865110/packages/npm/:auto-install-peers=true
```
## Inisiasi Projek
jalankan perintah
```npm install```
untuk menginstall semua kebutuhan projek


## Run Locally
jalankan perintah
```npm start```
untuk menjalankan project secara local

## Run Development Mode
jalankan perintah
```ng build``` atau ```npm run build```
untuk melakukan build projek, kemudian jalankan ```npm run preview``` untuk melihat hasil build pada port 4300

## Running unit tests
jalankan `ng test` untuk running unit testing via [Karma](https://karma-runner.github.io).


## Commit Rule
jalankan ```npm run commit``` untuk melakukan commit, kemudian ikuti petunjuk yang diminta.


## Live Preview
https://tsl-ases-dev.web.app/